import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers:[]
})
export class MenuComponent implements OnInit {
  navbarOpen = false;

  constructor() { }

  ngOnInit() {
  }

}

import { Component } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ChuckSwapiService } from 'src/data/chuck-swapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'chuck-swapi-frontend';
  people = [];
  jokes = [];
  randomJokes = [];

  searchTerm = '';
  page = 1;
  

  constructor(private service:ChuckSwapiService,private spinner: NgxSpinnerService){

  }
  ngOnInit() {

  }
  getPeople(){
    this.spinner.show();
    this.service.getPeople().subscribe((data:any) =>{
      this.people = data;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    },err =>{
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    });
  }
  getJokes(){
    this.spinner.show();
    this.service.getJokes().subscribe((data:any) =>{
      this.jokes = data;
      console.log(data);
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    },err =>{
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    });
  }
  search(){
    this.spinner.show();
    this.service.searchJokes(this.searchTerm).subscribe((data:any) =>{
      this.jokes = data;
      console.log(data);
      this.searchPeople();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    },err =>{
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    });
  }
  searchPeople(){
    this.service.searchPeople(this.searchTerm).subscribe((data:any) =>{
      this.people = data;
      console.log(data);

    },err =>{
      
    });
  }
  randomJoke(){
    this.spinner.show();
    this.service.getRandomJokes().subscribe((data:any) =>{
      this.randomJokes = data;
      console.log(data);
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    },err =>{
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 5000);
    });
  }
}

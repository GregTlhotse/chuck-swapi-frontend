import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChuckSwapiService {
  protected url: string = environment.url;

  constructor(private http: HttpClient) { }
  getJokes(){
    return this.http.get(this.url + 'Chuck/Categories');
  }
  getPeople(){
    return this.http.get(this.url + 'Swapi/People');
  }
  searchPeople(data){
    return this.http.get(this.url + 'Search/People/' + data);
  }
  searchJokes(data){
    return this.http.get(this.url + 'Search/Jokes/' + data);
  }
  getRandomJokes(){
    return this.http.get(this.url + 'Chuck/Jokes/Random');
  }
}

import { TestBed } from '@angular/core/testing';

import { ChuckSwapiService } from './chuck-swapi.service';

describe('ChuckSwapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChuckSwapiService = TestBed.get(ChuckSwapiService);
    expect(service).toBeTruthy();
  });
});
